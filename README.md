Rust Bloom
==========

*"A rust bloom is indicative of the chemical formation of new rust. It can be
either localized or uniform and widely spread across a surface."*
<small>([Corrosionpedia](https://www.corrosionpedia.com/definition/2310/rust-bloom))</small>

This is a collection of Rust example snippets that I struggled with or that I
might find useful some time in the future.

Feel free to to let yourself be inspired or to be amused by my ugly
Rust-beginner-code 😜