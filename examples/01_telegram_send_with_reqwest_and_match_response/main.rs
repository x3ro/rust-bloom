#[macro_use]
extern crate maplit;

use dotenv::dotenv;
use reqwest::Response;
use serde_json::Value;
use std::env;
use std::panic;

fn main() {
    // Load environment from `.env` file
    dotenv().ok();

    let res = send_mesage();

    // Result can be matched like this ...
    match &res {
        Ok(v) => println!("==> {:#?}", v),
        Err(e) => panic!("An error occured: {}", e),
    }

    // ... or unwraped.. This will panic automatically if `res` is of type `Err`
    // `x` must be `mut` because the `.json()` further down wants to borrow a
    // mutable reference.
    let mut x = res.unwrap();

    // Here the type is determined at the assignment and inside the match
    // You just have to look wheter it is `Ok` or `Err`.
    let json_a: Value = match x.json() {
        Ok(v) => v,
        Err(e) => {
            println!("Failed to parse json: {:?}\nERROR: {}", x.text(), e);
            Value::Null
        }
    };

    // Finally, this is the way to call a method with a type parameter
    // (`::<..>` is called "turbofish"?)
    // Now inside of `y` is a `Result` that can be used later.
    let json_b = x.json::<Value>();

    println!("Value a: {:?}", json_a);
    println!("Value b: {:?}", json_b);
}

fn send_mesage() -> Result<Response, Box<dyn std::error::Error>> {
    let chat_id: &str = &env::var("TELEGRAM_CHAT_ID").expect("Getting telegram chat id");
    let map = hashmap! {
        "chat_id"    => chat_id,
        "text"       => "Hello from Rust!",
        "parse_mode" => "Markdown",
    };
    let client = reqwest::Client::new();
    let token = env::var("TELEGRAM_TOKEN").expect("Getting telegram token");
    let res = client
        .post(&format!(
            "https://api.telegram.org/bot{}/sendMessage",
            token
        ))
        .json(&map)
        .send()?;
    return Ok(res);
}
