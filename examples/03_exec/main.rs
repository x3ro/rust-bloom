use std::error::Error;
use std::process::Command;
use std::os::unix::process::CommandExt;

fn main() -> Result<(), Box<dyn Error>> {
    println!("\nWithout `exec`, the command is executed as a child of this process:\n");
    Command::new("ps")
        .args(&["-o", "%c", "--forest", "--no-headers"])
        .spawn()?
        .wait()?;
    
    println!("\n\nWith exec, the new process replaces the current one:\n");
    Command::new("ps")
        .args(&["-o", "%c", "--forest", "--no-headers"])
        .exec();

    Ok(())
}