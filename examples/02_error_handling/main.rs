mod app {
    pub enum Error {
        Message(&'static str),
    }
    pub enum ExitCode {
        Ok = 0,
        Unexpected = -1,
    }

    pub type Result = std::result::Result<(), Error>;
}

fn main() {
    use app::Error::*;
    use app::ExitCode;

    let result = start();

    let exit_code = match result {
        Ok(()) => ExitCode::Ok,
        Err(e) => match e {
            Message(m) => {
                println!("ERROR: Unexpected exit with message: {}", m);
                ExitCode::Unexpected
            }
        },
    };

    std::process::exit(exit_code as i32);
}

fn start() -> app::Result {
    Err(app::Error::Message("bla"))
}
