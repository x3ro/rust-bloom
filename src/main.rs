fn main() {
    println!(
        "\n\
         This is just a collection of examples.\n\
         Use `cargo run --example` to get a list of available examples and\n\
         execute `cargo run --example <example-name>` to run an example!\n"
    );
}
